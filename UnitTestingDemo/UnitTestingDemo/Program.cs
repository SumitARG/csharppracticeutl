﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestingDemo
{
    class Program
    {

        public static List<SuperHero> GetFiveSuperHeroes()
        {
            //create a n empty list.
            var tempList = new List<SuperHero>();

            //five super heroes. 
            var name = "";
            var power = "";
            var brand = "";
            var age = 0;

            //five super heroes. 
            var tempHero1 = new SuperHero();
            var tempHero2 = new SuperHero();
            var tempHero3 = new SuperHero();
            var tempHero4 = new SuperHero();
            var tempHero5 = new SuperHero();
            var tempHero6 = new SuperHero();
            name = "Batman";
            power = "Money";
            brand = "DC";
            age = 30;

            tempHero1.name = name;
            tempHero1.power = power;
            tempHero1.brand = brand;
            tempHero1.age = age;

            //add this hero to list. 
            tempList.Add(tempHero1);


            name = "Thor";
            power = "God Of Thunder";
            brand = "Marvel";
            age = 36;

            tempHero2.name = name;
            tempHero2.power = power;
            tempHero2.brand = brand;
            tempHero2.age = age;

            //add this hero to list. 
            tempList.Add(tempHero2);

            name = "Hulk";
            power = "Smash";
            brand = "Marvel";
            age = 38;

            tempHero3.name = name;
            tempHero3.power = power;
            tempHero3.brand = brand;
            tempHero3.age = age;

            //add this hero to list. 
            tempList.Add(tempHero3);

            name = "Wonder Woman";
            power = "Goddess";
            brand = "DC";
            age = 100;

            tempHero4.name = name;
            tempHero4.power = power;
            tempHero4.brand = brand;
            tempHero4.age = age;

            //add this hero to list. 
            tempList.Add(tempHero4);

            name = "Ironman";
            power = "Millionair Genius Playboy";
            brand = "Marvel";
            age = 35;

            tempHero5.name = name;
            tempHero5.power = power;
            tempHero5.brand = brand;
            tempHero5.age = age;

            //add this hero to list. 
            tempList.Add(tempHero5);

            name = "Flash";
            power = "Speed";
            brand = "DC";
            age = 20;

            tempHero6.name = name;
            tempHero6.power = power;
            tempHero6.brand = brand;
            tempHero6.age = age;

            //add this hero to list. 
            tempList.Add(tempHero6);


            return tempList;
        }

        public static SuperHero findByAge(int age, List<SuperHero> finalList)
        {
            SuperHero temphero1 = new SuperHero();
            temphero1 = finalList.Select(x => x).Where(x => x.age == age).FirstOrDefault();

            return temphero1;
        }
            static void Main(string[] args)
        {
            var ListOfSuperHeroes = new List<SuperHero>();
            double num1 = 10.0;
            double num2 = 20.0;
            double result_of_addition = 0.0;
            result_of_addition = sumOfTwoNumber(num1, num2);
            ListOfSuperHeroes = GetFiveSuperHeroes();
            int age = 30;
            SuperHero returnHero = findByAge(age, ListOfSuperHeroes);
            Console.ReadLine();
        }

        public static double sumOfTwoNumber(double num1, double num2)
        {
            double result = num1 + num2;
            return result;

        }
    }
}
