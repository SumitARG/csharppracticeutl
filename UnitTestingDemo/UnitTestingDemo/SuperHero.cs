﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestingDemo
{
    class SuperHero
    {
        public String name { get; set; }
        public String power { get; set; }
        public String brand { get; set; }
        public int age { get; set; }

        public SuperHero(String name, String power, String brand, int age)
        {
            this.name = name;
            this.power = power;
            this.brand = brand;
            this.age = age;
        }

        public SuperHero()
        {
        }
    }
}
