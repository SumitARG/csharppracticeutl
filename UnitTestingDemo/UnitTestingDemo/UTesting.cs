﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestingDemo
{
    ////unit test for code reuse.
    //copy paste this code file to build new unit tests quickly.
    [TestClass]
    public class SampleTest
    {
        [TestMethod]
        public void TestSample1()
        {
            var actual = true;
            var expected = true;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestSample2()
        {
            var actual = true;
            var expected = false;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestSample3()
        {
            Program pro = new Program();
            var input1 = 8305.89;
            var input2 = 60895.98;
            var actual = 69201.87;
            var expected = Program.sumOfTwoNumber(input1,input2);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestSample4()
        {
            Program pro = new Program();
            var input1 = 8305.89;
            var input2 = 60895.98;
            var actual = Program.sumOfTwoNumber(input1, input2);
            var expected = 69201.87;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestSample5()
        {
            Program pro = new Program();
            var input1 = 358132.32;
            var input2 = 958321.69;
            var actual = Program.sumOfTwoNumber(input1, input2);
            var expected = 1316454.01;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestSample6()
        {
            Program pro = new Program();
            int age = 30;
            SuperHero actualHero = Program.findByAge(age, Program.GetFiveSuperHeroes());
            
            SuperHero expectedHero = new SuperHero("Batman","Money","DC",30);
            bool actual = false;
            bool expected = true;
            if (expectedHero.age == actualHero.age && expectedHero.name == actualHero.name && expectedHero.brand == actualHero.brand && expectedHero.power == actualHero.power)
                actual = true;
            Assert.AreEqual(expected, actual);

        }

        [TestMethod]
        public void TestSample7()
        {
            Program pro = new Program();
            int age = 30;
            SuperHero actualHero = Program.findByAge(age, Program.GetFiveSuperHeroes());

            SuperHero expectedHero = new SuperHero("Shaktiman", "Money", "DC", 30);
            bool actual = false;
            bool expected = true;
            if (expectedHero.age == actualHero.age && expectedHero.name == actualHero.name && expectedHero.brand == actualHero.brand && expectedHero.power == actualHero.power)
                actual = true;
            Assert.AreEqual(expected, actual);

        }
    }
    class UTesting
    {
    }
}
